import pandas as pd
import openpyxl as px
import json
import sys

def get_formatted_df(filename):
    df = pd.read_csv(filename)
    formatted_df = df.T
    formatted_df.columns = formatted_df.iloc[0,:].values
    formatted_df = formatted_df.iloc[1:,:]
    return formatted_df


def populate_sheet_adjacent_cols(wkbook, sheetval, sheetpos):
    '''
        Populates a workbook sheet based on the position of the first column
        and continues until the nth (or ncols) adjacent column.

        The adjacent column calculation is limited to single letters for now.

        No need to return since the mutation in wkbook is retained after the function's execution.
    '''
    for col in list(sheetpos.keys())[1:]:
        
        insert_val = sheetval[col] if col in sheetval.columns else [0] * len(sheetval.index)
        
        for i in range(len(sheetval.index)):
            posn = sheetpos[col]
            letterval = chr(ord(posn[0])+i)
            wkbook[sheetpos['sheetname']][letterval + posn[1:]] = insert_val[i]

def fill_excel_workbook_2sheets():
    
    s1 = get_formatted_df(sys.argv[1])
    s2 = get_formatted_df(sys.argv[2])

    with open(sys.argv[3], 'r') as f:
        sheet1_pos = json.load(f)
    
    with open(sys.argv[4], 'r') as f:
        sheet2_pos = json.load(f)

    wb = px.load_workbook(sys.argv[5])

    populate_sheet_adjacent_cols(wb, s1, sheet1_pos)
    populate_sheet_adjacent_cols(wb, s2, sheet2_pos)

    wb.save(sys.argv[6])
    wb.close()


if __name__ == "__main__":
    fill_excel_workbook_2sheets()